const express = require('express');
const router = express.Router();

//allows to use productController.js
const productController = require("../controllers/productController");


//Route to post a product
router.post('/', (req, res)=>{
	productController.createProduct(req.body)
	.then(resultFromController => res.send(resultFromController))
})


//Route for getting all products
router.get('/',(req,res)=>{
	productController.getAllProducts()
	.then(resultFromController => res.send(resultFromController))
})


//Route to get specific product through ID
router.get('/:id',(req,res)=>{
	productController.getProduct(req.params.id)
	.then(resultFromController => res.send(resultFromController))
})

//DELETE product by ID
router.delete('/delete/:id',(req,res)=>{
		productController.deleteProduct(req.params.id)
		.then(resultFromController => res.send(resultFromController))

})

module.exports = router;