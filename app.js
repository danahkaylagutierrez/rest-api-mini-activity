const express = require('express');
const mongoose = require ('mongoose');

//This allows to use all the  in 'productRoute.js'
const productRoute= require('./routes/productRoute')

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended :true}));

//Connect to mongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.r2a4x.mongodb.net/s31?retryWrites=true&w=majority',
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	});

app.use('/products',productRoute)



//Server listening
app.listen(port, ()=> console.log(`Server running at port ${port}`))